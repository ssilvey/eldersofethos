//
//  AppDelegate.h
//  EldersOfEthos
//
//  Created by Steven Silvey on 6/26/15.
//
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

