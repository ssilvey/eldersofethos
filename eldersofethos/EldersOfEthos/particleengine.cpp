//
//  particleengine.cpp
//  eldersofethos
//
//  Created by Steven Silvey on 6/27/15.
//
//

#include "particleengine.h"

#define MAX_PARTICLES   1000

ParticleEngine::ParticleEngine() {
    this->arrParticles = new Particle*[MAX_PARTICLES];
    for(int ndx = 0; ndx < MAX_PARTICLES; ndx++) {
        this->arrParticles[ndx] = NULL;
    }
}

void ParticleEngine::CreateParticle(PARTICLESPIRIT spirtType) {
    Particle *newParticle = new Particle(spirtType);
    
    int ndx = 0;
    while(ndx < MAX_PARTICLES && this->arrParticles[ndx] != NULL) {
        ndx++;
    }
    
    if(ndx < MAX_PARTICLES) {
        this->arrParticles[ndx] = newParticle;
        this->count++;
    }
}

void ParticleEngine::Draw() {
    for(int ndx = 0; ndx < MAX_PARTICLES; ndx++) {
        if(this->arrParticles[ndx]) {
            this->arrParticles[ndx]->Draw();
        }
    }
}

void ParticleEngine::Update(int maxCount, int ghosts, int angels) {
    for(int ndx = 0; ndx < MAX_PARTICLES; ndx++) {
        if(this->arrParticles[ndx]) {
            if(!this->arrParticles[ndx]->alive) {
                delete this->arrParticles[ndx];
                this->arrParticles[ndx] = NULL;
                this->count--;
            }
            else {
                this->arrParticles[ndx]->Update();
            }
            
        }
        
    }
    if(this->count < (maxCount + ghosts + angels) && this->count < MAX_PARTICLES && (int)(arc4random() % 100) >= 80) {
        int choose = arc4random() % (maxCount + ghosts + angels);
        if(choose < maxCount) {
            this->CreateParticle(PARTICLESPIRIT_NORMAL);
        }
        else {
            choose -= maxCount;
            if(choose < ghosts) {
                this->CreateParticle(PARTICLESPIRIT_GHOST);
            }
            else {
                choose -= ghosts;
                if(choose < angels) {
                    this->CreateParticle(PARTICLESPIRIT_ANGEL);
                }
            }
        }
        
    }
}
