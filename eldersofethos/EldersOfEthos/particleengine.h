//
//  particleengine.h
//  eldersofethos
//
//  Created by Steven Silvey on 6/27/15.
//
//

#ifndef __eldersofethos__particleengine__
#define __eldersofethos__particleengine__

#include <stdio.h>
#include "particle.h"

class ParticleEngine {
public:
    
    ParticleEngine();
    void Draw();
    void Update(int maxCount, int ghosts, int angels);
    
private:
    Particle **arrParticles;
    int count;
    
    
    void CreateParticle(PARTICLESPIRIT spirtType);
    
};

#endif /* defined(__eldersofethos__particleengine__) */
