//
//  tribe.cpp
//  eldersofethos
//
//  Created by Steven Silvey on 6/26/15.
//
//

#include <algorithm>
#include <allegro5/allegro_primitives.h>

#include "tribe.h"
#include "game.h"


#define MAX_PLANES 4


/// Separated by 74
#define PLANE_HEAVEN_YPOS           146
#define PLANE_SELFACTUAL_YPOS       220
#define PLANE_PHYSIOLOGICAL_YPOS    294
#define PLANE_PURGATORY_YPOS        368

Tribe::Tribe(){
    arrPlanes = new Plane[MAX_PLANES];
    arrPlanes[PLANE_PHYSIOLOGICAL].available = 30;
    arrPlanes[PLANE_PHYSIOLOGICAL].unwilling = 15;
    this->isCPU = false;
    
    this->angel = new Parameter("heavenIcon.png");
    
    this->purity = new Parameter("purityIcon.png");
    this->evil = new Parameter("evilIcon.png");
    
    this->energy = new Parameter("energyIcon.png");
    this->meditation = new Parameter("medIcon.png");
    this->guidance = new Parameter("guidanceIcon.png");
    this->decay = new Parameter("decayIcon.png");
    
    this->death = new Parameter("ghostsIcon.png");
    
    this->energy->stock = 5;
    this->meditation->stock = 5;
    this->guidance->stock = 5;
    this->UpdateProgress();
    
    
}

void Tribe::Draw(){
    Game *game;
    int ypos = 300;
    
    al_draw_line(0, PLANE_HEAVEN_YPOS, SCREEN_W, PLANE_HEAVEN_YPOS, FONT_BASIC_COLOR, 1.0f);
    al_draw_line(0, PLANE_SELFACTUAL_YPOS, SCREEN_W, PLANE_SELFACTUAL_YPOS, FONT_BASIC_COLOR, 1.0f);
    al_draw_line(0, PLANE_PHYSIOLOGICAL_YPOS, SCREEN_W, PLANE_PHYSIOLOGICAL_YPOS, FONT_BASIC_COLOR, 1.0f);
    al_draw_line(0, PLANE_PURGATORY_YPOS, SCREEN_W, PLANE_PURGATORY_YPOS, FONT_BASIC_COLOR, 1.0f);

    al_draw_text(game->getInstance().font, FONT_BASIC_COLOR, 10, PLANE_PURGATORY_YPOS+20,ALLEGRO_ALIGN_LEFT, "Purgatory:");
    al_draw_text(game->getInstance().font, FONT_BASIC_COLOR, 10, PLANE_PHYSIOLOGICAL_YPOS+20,ALLEGRO_ALIGN_LEFT, "Physilogical:");
    al_draw_text(game->getInstance().font, FONT_BASIC_COLOR, 10, PLANE_SELFACTUAL_YPOS+20,ALLEGRO_ALIGN_LEFT,    "Self-");
    al_draw_text(game->getInstance().font, FONT_BASIC_COLOR, 10, PLANE_SELFACTUAL_YPOS+38,ALLEGRO_ALIGN_LEFT,    "Actualization:");
    al_draw_text(game->getInstance().font, FONT_BASIC_COLOR, 10, PLANE_HEAVEN_YPOS+20,ALLEGRO_ALIGN_LEFT,    "Heaven:");
    
    
    this->angel->Draw("Angels", 155, PLANE_HEAVEN_YPOS+5);
    
    this->purity->Draw("Purity", 155, PLANE_SELFACTUAL_YPOS+5);
    this->evil->Draw("Evil", 230, PLANE_SELFACTUAL_YPOS+5);
    
    this->energy->Draw("Energy", 155, PLANE_PHYSIOLOGICAL_YPOS+5);
    this->meditation->Draw("Meditation", 230, PLANE_PHYSIOLOGICAL_YPOS+5);
    this->guidance->Draw("Guidance", 305, PLANE_PHYSIOLOGICAL_YPOS+5);
    this->decay->Draw("Decay", 380, PLANE_PHYSIOLOGICAL_YPOS+5);
    
    this->death->Draw("Ghosts", 155, PLANE_PURGATORY_YPOS+5);


    for(int ndx = 0; ndx < MAX_PLANES; ndx++) {
        ypos = PLANE_PURGATORY_YPOS-ndx*74;
        
        if(ndx != PLANE_PURGATORY && ndx != PLANE_HEAVEN) {
            Plane current = arrPlanes[ndx];
            current.DrawSpiritsInfoAt(ypos);
            
        }
        
        
    }
}

void Tribe::KillRandomSpirit() {
    if(this->SpiritCount() <= 0) {
        return;
    }
    
    int breakOut = 0;
    bool foundSpirit = false;
    while(!foundSpirit && breakOut < 200) {
        breakOut++;
        if((arc4random() % 100 < 20 || breakOut > 100) && this->purity->spirits > 0) {
            this->purity->spirits--;
            foundSpirit = true;
        }
        else if((arc4random() % 100 < 20 || breakOut > 100) && this->meditation->spirits > 0) {
            this->meditation->spirits--;
            foundSpirit = true;
        }
        else if((arc4random() % 100 < 20 || breakOut > 100) && this->guidance->spirits > 0) {
            this->guidance->spirits--;
            foundSpirit = true;
        }
        else if((arc4random() % 100 < 20 || breakOut > 100) && this->energy->spirits > 0) {
            this->energy->spirits--;
            foundSpirit = true;
        }
        
        else if((arc4random() % 100 < 20 || breakOut > 100) && this->arrPlanes[PLANE_PHYSIOLOGICAL].available > 0) {
            this->arrPlanes[PLANE_PHYSIOLOGICAL].available--;
            foundSpirit = true;
            if(this->arrPlanes[PLANE_PHYSIOLOGICAL].available < this->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling) {
                this->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling = this->arrPlanes[PLANE_PHYSIOLOGICAL].available;
            }
            if(this->arrPlanes[PLANE_PHYSIOLOGICAL].available < this->arrPlanes[PLANE_PHYSIOLOGICAL].ready) {
                this->arrPlanes[PLANE_PHYSIOLOGICAL].ready = this->arrPlanes[PLANE_PHYSIOLOGICAL].available;
            }
        }
        else if((arc4random() % 100 < 20 || breakOut > 100) && this->arrPlanes[PLANE_SELFACTUAL].available > 0) {
            this->arrPlanes[PLANE_PHYSIOLOGICAL].available--;
            foundSpirit = true;
            if(this->arrPlanes[PLANE_PHYSIOLOGICAL].available < this->arrPlanes[PLANE_SELFACTUAL].unwilling) {
                this->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling = this->arrPlanes[PLANE_SELFACTUAL].available;
            }
            if(this->arrPlanes[PLANE_PHYSIOLOGICAL].available < this->arrPlanes[PLANE_SELFACTUAL].ready) {
                this->arrPlanes[PLANE_PHYSIOLOGICAL].ready = this->arrPlanes[PLANE_SELFACTUAL].available;
            }
        }
    }


}

bool Tribe::RandomlyDetermineUnwillingSpirit() {
    int choose = arc4random() % 50;
    if(choose < 10 && this->energy->spirits > 0) {
        this->energy->spirits--;
        this->arrPlanes[PLANE_PHYSIOLOGICAL].available++;
        this->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling++;
    }
    else if(choose >= 10 && choose < 20 && this->meditation->spirits > 0) {
        this->meditation->spirits--;
        this->arrPlanes[PLANE_PHYSIOLOGICAL].available++;
        this->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling++;
    }
    else if(choose >= 20 && choose < 30 && this->guidance->spirits > 0) {
        this->guidance->spirits--;
        this->arrPlanes[PLANE_PHYSIOLOGICAL].available++;
        this->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling++;
    }
    else if(choose >= 30 && choose < 40 && this->purity->spirits > 0) {
        this->purity->spirits--;
        this->arrPlanes[PLANE_SELFACTUAL].available++;
        this->arrPlanes[PLANE_SELFACTUAL].unwilling++;
    }
    else if(this->arrPlanes[PLANE_PHYSIOLOGICAL].available - this->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling > 0) {
        this->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling++;
    }
    else {
        return false;
    }
    
    return true;
}

bool Tribe::RandomlyDetermineWillingSpirit() {
    if(this->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling > 0) {
        this->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling--;
    }
    else if(this->arrPlanes[PLANE_SELFACTUAL].unwilling > 0) {
        this->arrPlanes[PLANE_SELFACTUAL].unwilling--;
    }
    else {
        return false;
    }
    
    return true;
}

int Tribe::AssignSpirits(int count, TRIBEPARAMETER from, TRIBEPARAMETER to) {
    int assignCount = count;
    int plane = 0;
    
    if(from == TRIBE_PARAMETER_NONE || to == TRIBE_PARAMETER_NONE) {
        Game *game;
        game->getInstance().AddMessage(MSGSPEAKER_MASLOW, "Are you sure you spoke (typed) the group name correctly?");
        return 0;
    }
    
    switch(from) {
        case TRIBE_PARAMETER_NONE:
            return 0;
            break;
        case TRIBE_PARAMETER_EARTH:
            break;
        case TRIBE_PARAMETER_ANY:
            for(int ndx = 0; ndx < count; ndx++) {
                KillRandomSpirit();
            }
            break;
        case TRIBE_PARAMETER_AVAILABLE:
            plane = this->DeterminePlane(to);
            if(this->arrPlanes[plane].available - count < this->arrPlanes[plane].unwilling) {
                assignCount = count - this->arrPlanes[plane].unwilling;
            }
            
            if(assignCount > this->arrPlanes[plane].available) {
                assignCount = this->arrPlanes[plane].available;
            }
            if(assignCount < 0) {
                assignCount = 0;
            }
            this->arrPlanes[plane].available -= assignCount;
            this->arrPlanes[plane].assigned += assignCount;
            break;
        case TRIBE_PARAMETER_ENERGY:
            if (assignCount > this->energy->spirits) {
                assignCount = this->energy->spirits;
            }
            
            this->energy->spirits -= assignCount;
            break;
        case TRIBE_PARAMETER_MEDITATION:
            if (assignCount > this->meditation->spirits) {
                assignCount = this->meditation->spirits;
            }
            
            this->meditation->spirits -= assignCount;
            break;
        case TRIBE_PARAMETER_GUIDANCE:
            if (assignCount > this->guidance->spirits) {
                assignCount = this->guidance->spirits;
            }
            
            this->guidance->spirits -= assignCount;
            break;
        case TRIBE_PARAMETER_PURITY:
            if (assignCount > this->purity->spirits) {
                assignCount = this->purity->spirits;
            }
            
            this->purity->spirits -= assignCount;
            break;
        default:
            break;
    }
    
    switch(to) {
        case TRIBE_PARAMETER_AVAILABLE:
            plane = this->DeterminePlane(from);
            this->arrPlanes[plane].available += assignCount;
            this->arrPlanes[plane].assigned -= assignCount;
            break;
        case TRIBE_PARAMETER_ENERGY:
            this->energy->spirits += assignCount;
            break;
        case TRIBE_PARAMETER_MEDITATION:
            this->meditation->spirits += assignCount;
            break;
        case TRIBE_PARAMETER_GUIDANCE:
            this->guidance->spirits += assignCount;
            break;
        case TRIBE_PARAMETER_PURITY:
            this->purity->spirits += assignCount;
            break;
        case TRIBE_PARAMETER_DEATH:
            this->death->spirits += assignCount;
            break;
        case TRIBE_PARAMETER_HEAVEN:
            this->angel->spirits += assignCount;
            break;
        default:
            break;
    }
    
    
    this->UpdateProgress();
    return assignCount;
}



TRIBEPARAMETER Tribe::FindParameterInUstrInRange(ALLEGRO_USTR *ustr, int start, int end) {
    TRIBEPARAMETER retVal = TRIBE_PARAMETER_NONE;
    
    int posOfEnergy =al_ustr_find_cstr(ustr, start+1, "energy");
    if(posOfEnergy > start && posOfEnergy < end) {
        retVal = TRIBE_PARAMETER_ENERGY;
    }
    
    int posOfAvailable =al_ustr_find_cstr(ustr, start+1, "available");
    if(posOfAvailable > start && posOfAvailable < end) {
        retVal = TRIBE_PARAMETER_AVAILABLE;
    }
    
    int posOfMeditation =al_ustr_find_cstr(ustr, start+1, "meditation");
    if(posOfMeditation > start && posOfMeditation < end) {
        retVal = TRIBE_PARAMETER_MEDITATION;
    }
    
    int posOfGuidance =al_ustr_find_cstr(ustr, start+1, "guidance");
    if(posOfGuidance > start && posOfGuidance < end) {
        retVal = TRIBE_PARAMETER_GUIDANCE;
    }
    
    int posOfPurity =al_ustr_find_cstr(ustr, start+1, "purity");
    if(posOfPurity > start && posOfPurity < end) {
        retVal = TRIBE_PARAMETER_PURITY;
    }
    
    return retVal;
}

void Tribe::UpdateParameters() {
    for(int ndx = 0; ndx < MAX_PLANES; ndx++) {
        if(ndx == PLANE_PHYSIOLOGICAL) {
            this->UpdateEnergy();
            this->UpdateMeditation();
            this->UpdateGuidance();
            this->UpdatePurity();
        }
    }
}

void Tribe::UpdateEnergy() {
    /// Decay
    this->energy->stock -= this->energy->spirits;
    this->energy->stock -= this->guidance->spirits;
    this->energy->stock -= this->purity->spirits*2;
    this->energy->stock -= this->decay->spirits;
    
    Game *game;
    if(game->getInstance().gameWon) {
        this->energy->stock -= arc4random() % 10;
    }
    
    /// Growth
    this->energy->stock += this->energy->spirits * 2.5;
}

void Tribe::UpdateMeditation() {
    /// Decay
    this->meditation->stock -= this->SpiritCount()/4+1;
    this->meditation->stock -= this->purity->spirits*2;
    this->meditation->stock -= this->decay->spirits;
    
    Game *game;
    if(game->getInstance().gameWon) {
        this->meditation->stock -= arc4random() % 10;
    }

    
    /// Growth
    this->meditation->stock += this->meditation->spirits*2;
}

void Tribe::UpdateGuidance() {
    /// Decay
    this->guidance->stock -= 6;
    this->guidance->stock -= this->decay->spirits;
    
    Game *game;
    if(game->getInstance().gameWon) {
        this->guidance->stock -= arc4random() % 10;
    }

    
    /// Growth
    this->guidance->stock += this->guidance->spirits*2;
}

void Tribe::UpdatePurity() {
    if(this->purity->stock > 0 || this->purity->spirits > 0 || this->arrPlanes[PLANE_SELFACTUAL].available > 0) {
        /// Decay
        this->purity->stock -= 3 + this->death->spirits / 2;
        this->purity->stock -= this->decay->spirits;
        
        /// Growth
        this->purity->stock += this->purity->spirits;
        
    }
}

void Tribe::UpdateProgress() {
    this->energy->progress = (float)this->energy->stock / 20.0f;
    if(this->energy->progress > 1.0) {
        this->energy->progress = 1.0f;
    }
    if(this->energy->progress < 0.0f) {
        this->energy->progress = 0.0f;
    }
    
    
    this->meditation->progress = (float)this->meditation->stock / this->SpiritCount()*2;
    if(this->meditation->progress > 1.0) {
        this->meditation->progress = 1.0f;
    }
    if(this->meditation->progress < 0.0f) {
        this->meditation->progress = 0.0f;
    }
    
    
    this->guidance->progress = (float)this->guidance->stock / this->SpiritCount()*2;
    if(this->guidance->progress > 1.0) {
        this->guidance->progress = 1.0f;
    }
    if(this->guidance->progress < 0.0f) {
        this->guidance->progress = 0.0f;
    }
    
    this->death->progress = 1.0f - (float)this->death->spirits / 5.0f;
    if(this->death->progress > 1.0) {
        this->death->progress = 1.0f;
    }
    if(this->death->progress < 0.0f) {
        this->death->progress = 0.0f;
    }

    
    this->decay->progress = 1.0f - (float)this->decay->spirits / 5.0f;
    if(this->decay->progress > 1.0) {
        this->decay->progress = 1.0f;
    }
    if(this->decay->progress < 0.0f) {
        this->decay->progress = 0.0f;
    }

    this->purity->progress = (float)this->purity->stock / 20.0f;
    if(this->purity->progress > 1.0) {
        this->purity->progress = 1.0f;
    }
    if(this->purity->progress < 0.0f) {
        this->purity->progress = 0.0f;
    }
    
    this->angel->progress = (float)this->angel->spirits / 10.0f;
    if(this->angel->progress > 1.0) {
        this->angel->progress = 1.0f;
    }
    if(this->angel->progress < 0.0f) {
        this->angel->progress = 0.0f;
    }
    
}

const char* Tribe::DetermineEvent() {
    int start = 0;
    int range = 0;
    int ndx = 0;
    EventChance **arrPossibleEvents = new EventChance*[30];
    
    this->UpdateProgress();
    
    
    if(this->energy->stock < 0 && this->SpiritCount() > 0) {
        /// Death from low energy
        int chance =(abs(this->energy->stock)*2);
        range += chance;
        EventChance *death = new EventChance(EVENTCODE_DEATH, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = death;

    }
    if(this->energy->stock > 0) {
        /// Guidance Boost from high energy
        int chance =3;
        range += chance;
        EventChance *guidanceBoost = new EventChance(EVENTCODE_GUIDANCE_BOOST, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = guidanceBoost;
    }
    if(this->energy->stock > 0) {
        /// Meditation Boost from high energy
        int chance =3;
        range += chance;
        EventChance *meditationBoost = new EventChance(EVENTCODE_MEDITATION_BOOST, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = meditationBoost;
    }
    if(this->meditation->progress < 0.8f) {
        /// Unwillingness due to low meditation
        int chance =10;
        range += chance;
        EventChance *newUnwillingness = new EventChance(EVENTCODE_NEW_UNWILLINGNESS, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = newUnwillingness;
    }
    if(this->meditation->progress >= 0.8f && (this->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling > 0 || this->arrPlanes[PLANE_SELFACTUAL].unwilling > 0)) {
        /// Willingness due to high meditation
        int chance =40;
        range += chance;
        EventChance *newWillingness = new EventChance(EVENTCODE_NEW_WILLINGNESS, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = newWillingness;
    }
    if(this->guidance->progress < 0.5f) {
        /// Spirits leaving due to low guidance
        int chance =10;
        range += chance;
        EventChance *newUnwillingness = new EventChance(EVENTCODE_SPIRITS_MIGRATING, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = newUnwillingness;
    }
    
    if(this->guidance->stock >= this->SpiritCount()*2) {
        /// Spirits coming due to high guidance
        int chance =30;
        range += chance;
        EventChance *newUnwillingness = new EventChance(EVENTCODE_NEW_SPIRITS, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = newUnwillingness;
    }
    
    if(this->death->spirits > 0 || this->SpiritCount() <= 0) {
        /// Ghosts can make you lose the game!
        int chance =this->death->spirits / 2;
        range += chance;
        EventChance *loseGame = new EventChance(EVENTCODE_LOSE_GAME, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = loseGame;
    }
    if(this->guidance->progress > 0.8f && this->energy->progress > 0.8f && this->meditation->progress > 0.8f) {
        /// Somebody might be ready for promotion!
        int chance = 40;
        range += chance;
        EventChance *spiritsReady = new EventChance(EVENTCODE_SPIRITS_READY, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = spiritsReady;
    }
    
    
    Game *game;
    if(this->purity->progress > 0.75f && !game->getInstance().gameWon) {
        /// Somebody might ascend to Heaven!
        int chance =30;
        range += chance;
        EventChance *ascend = new EventChance(EVENTCODE_ASCEND, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = ascend;
    }
    
    if(this->angel->spirits >= 10 && !game->getInstance().gameWon) {
        /// Somebody might ascend to Heaven!
        int chance =10000;  ///Yes, it's possible this doesn't happen...it will soon though
        range += chance;
        EventChance *win = new EventChance(EVENTCODE_WIN_GAME, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = win;
    }
    
    
    
    /// Events that can just...happen
    if(true) {
        int chance = 2;
        range += chance;
        EventChance *newGhost = new EventChance(EVENTCODE_NEW_GHOST, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = newGhost;
    }
    
    if(true) {
        int chance = 4;
        range += chance;
        EventChance *rift = new EventChance(EVENTCODE_COSMIC_RIFT, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = rift;
    }
    
    if(true) {
        int chance = 5;
        range += chance;
        EventChance *decay = new EventChance(EVENTCODE_DECAY_TRIBE, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = decay;
    }


    
    if(true) {
        /// Sometimes, nothing happens
        int chance =15;
        range += chance;
        EventChance *nothing = new EventChance(EVENTCODE_NOTHING, start, range);
        start = range+1;
        arrPossibleEvents[ndx++] = nothing;
    }
    
    int length = ndx;
    EventChance *selectEvent = NULL;
    int choose = arc4random() % range;
    for(int j = 0; j < length; j++) {
        EventChance *reviewEvent = arrPossibleEvents[j];
        if(choose >= reviewEvent->start && choose < reviewEvent->end) {
            selectEvent = reviewEvent;
            break;
        }
    }
    
    if(selectEvent) {
        const char *msg = this->ProcessEvent(selectEvent);
        this->UpdateProgress();
        
        return msg;
    }
    
    return NULL;
}

const char* Tribe::ProcessEvent(EventChance *event) {
    int arg = 0, arg2 = 0;
    int deaths = 0;
    Game *game;
    switch (event->code) {
        case EVENTCODE_NOTHING:
            return "Nothing has happened.";
            break;
        case EVENTCODE_DEATH:
            deaths = arc4random() % 4 + 1;
            if(deaths > this->SpiritCount()) {
                deaths = this->SpiritCount();
            }
            this->AssignSpirits(deaths, TRIBE_PARAMETER_ANY, TRIBE_PARAMETER_DEATH);
            
            return al_cstr(al_ustr_newf("Elder!  We do not have enough energy to sustain our tribe!  %d have fallen  into purgatory!", deaths));
            
            break;
       case EVENTCODE_GUIDANCE_BOOST:
            arg = arc4random() % 4+10;
            this->guidance->stock += arg;
            
            return "Good news, Elder.  The spirits are overflowing with ENERGY which has them working harder than ever, giving a small boost to GUIDANCE.";
            break;
        case EVENTCODE_NEW_UNWILLINGNESS:
            arg = arc4random() % 4+1;
            for(int ndx = 0; ndx < arg; ndx++) {
                if(!this->RandomlyDetermineUnwillingSpirit()) {
                    arg2++;
                }
            }
            return al_cstr(al_ustr_newf("Unfortunate news.  The tribe needs rest and MEDITATION to restore its      desire to follow your lead.  %d spirits have become UNWILLING, Elder.", (arg) - arg2));
            break;
       case EVENTCODE_NEW_WILLINGNESS:
            arg = arc4random() % 4+1;
            for(int ndx = 0; ndx < arg; ndx++) {
                if(!this->RandomlyDetermineWillingSpirit()) {
                    arg2++;
                }
            }
            return al_cstr(al_ustr_newf("Improvement, Elder.  The tribe has informed me they feel rested with the MEDITATION you've provided. %d spirits have become WILLING.", (arg) - arg2));
            break;
        case EVENTCODE_SPIRITS_MIGRATING:
            arg = arc4random() % 4+1;
            if(arg > this->SpiritCount()) {
                arg = this->SpiritCount();
            }
            this->AssignSpirits(arg, TRIBE_PARAMETER_ANY, TRIBE_PARAMETER_OTHER);
            
            return al_cstr(al_ustr_newf("We need to focus on GUIDANCE to bring lost souls to our tribe, Elder.  Several spirits have left our tribe to a better guided one!"));
            break;
        case EVENTCODE_NEW_SPIRITS:
            arg = arc4random() % 4+1;
            this->AssignSpirits(arg, TRIBE_PARAMETER_EARTH, TRIBE_PARAMETER_AVAILABLE);
            
            return al_cstr(al_ustr_newf("Lost souls have found their way to us, thanks to your GUIDANCE, Elder.  %d new spirits have joined our tribe.", arg));
        case EVENTCODE_MEDITATION_BOOST:
            arg = arc4random() % 4+10;
            this->meditation->stock += arg;
            
            return "Some of the surplus of ENERGY has helped us work through weariness, providing a small boost to MEDITATION.";
            break;
        case EVENTCODE_LOSE_GAME:
            game->getInstance().gameover = true;
            return "Elder...the GHOSTS coming from our tribe have managed to close the gates to HEAVEN.  We will have to wait 1000 years before we can again attempt to bring 10 deserving tribe-spirits to the final plane. (YOU HAVE LOST).";
            break;
            
        case EVENTCODE_SPIRITS_READY:
            arg = arc4random() % 3+1;
            if(arg + this->arrPlanes[PLANE_PHYSIOLOGICAL].ready > this->arrPlanes[PLANE_PHYSIOLOGICAL].available + this->energy->spirits + this->guidance->spirits + this->meditation->spirits) {
                arg = this->arrPlanes[PLANE_PHYSIOLOGICAL].available + this->energy->spirits + this->guidance->spirits + this->meditation->spirits - this->arrPlanes[PLANE_PHYSIOLOGICAL].ready;
            }
            this->arrPlanes[PLANE_PHYSIOLOGICAL].ready += arg;
            return "Elder, we have spirits ready to ascend to the plane of SELF-ACTUALIZATION.  Make spirits available with the 'FREE' command and then move them up with the 'PROMOTE' command.";
            break;
        case EVENTCODE_ASCEND:
            arg = arc4random() % 3+1;
            if(arg > this->arrPlanes[PLANE_SELFACTUAL].available + this->purity->spirits) {
                arg = this->arrPlanes[PLANE_SELFACTUAL].available + this->purity->spirits;
            }
            if(this->angel->spirits + arg > 10) {
                arg = 10 - this->angel->spirits;
            }
            this->AssignSpirits(arg, TRIBE_PARAMETER_PURITY, TRIBE_PARAMETER_HEAVEN);
            return al_cstr(al_ustr_newf("Elder, I've just been informed!  %d spirits have been accepted and passed on to HEAVEN!", arg));
            break;

            break;
        case EVENTCODE_WIN_GAME:
            game->getInstance().gameWon = true;
            return "Elder!  ELDER!  Ten spirits from our tribe have earned their wings and passed into HEAVEN!  It's unfortunate we cannot send more, but the gates have already shut.  Our tribe will be the envy of Ethos and many will seek us when the gates open again! (YOU WIN!)";
            
            break;
        case EVENTCODE_NEW_GHOST:
            this->death->spirits++;
            
            return "A GHOST has appeared!  It's not from our tribe...I can't explain this, sometimes they just appear.";
            break;
        case EVENTCODE_COSMIC_RIFT:
            this->energy->stock = 3;
            
            return "A cosmic rift has depleted our ENERGY!  There was nothing we could do to prepare for this...";
            break;
        case EVENTCODE_DECAY_TRIBE:
            this->decay->spirits++;
            
            return "Another tribe is attacking us via DECAY, reducing our efficiency across disciplines.";
            break;
        default:
            break;
    }
    
    
    return NULL;
}

int Tribe::SpiritCount() {
    return this->energy->spirits +
    this->meditation->spirits +
    this->guidance->spirits +
    this->AvailableSpiritCount();
}

int Tribe::AvailableSpiritCount() {
    int availableCount = 0;
    for(int ndx = 0; ndx < MAX_PLANES; ndx++) {
        availableCount += this->arrPlanes[ndx].available;
    }
    
    return availableCount;
}


//////////////////////////////////////////////////////////////////////////////

int Tribe::DeterminePlane(TRIBEPARAMETER parameter) {
    switch(parameter) {
        case TRIBE_PARAMETER_ENERGY:
        case TRIBE_PARAMETER_MEDITATION:
        case TRIBE_PARAMETER_GUIDANCE:
        case TRIBE_PARAMETER_EARTH:
        case TRIBE_PARAMETER_NONE:
            return PLANE_PHYSIOLOGICAL;
            break;
        case TRIBE_PARAMETER_DEATH:
            return PLANE_PURGATORY;
            break;
        case TRIBE_PARAMETER_PURITY:
            return PLANE_SELFACTUAL;
            break;
        default:
            break;
    }
    
    return 0;
}
