//
//  parameter.h
//  eldersofethos
//
//  Created by Steven Silvey on 6/27/15.
//
//

#ifndef __eldersofethos__parameter__
#define __eldersofethos__parameter__

#include <allegro5/allegro.h>
#include <stdio.h>

class Parameter {
public:
    int stock;
    int spirits;
    float progress;
    ALLEGRO_BITMAP *icon;
    
    
    Parameter(const char *iconFileName);
    void Draw(const char *caption, int x, int y);
private:
};

#endif /* defined(__eldersofethos__parameter__) */
