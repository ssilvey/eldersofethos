//
//  main.m
//  EldersOfEthos
//
//  Created by Steven Silvey on 6/26/15.
//
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
