//
//  game.h
//  eldersofethos
//
//  Created by Steven Silvey on 6/26/15.
//
//

#ifndef __eldersofethos__game__
#define __eldersofethos__game__

#include <stdio.h>
#include "allegro5/allegro_font.h"
#include "particleengine.h"

#define FONT_TITLE_COLOR       al_map_rgb(0,237,191)
#define FONT_BASIC_COLOR        al_map_rgb(18,107,238)
#define FONT_ELDER_SPEAKING         al_map_rgb(0,141,113)
#define FONT_ELDERNAME_SPEAKING     al_map_rgb(0,237,191)
#define FONT_MASLOW_SPEAKING        al_map_rgb(165,131,0)
#define FONT_MASLOWNAME_SPEAKING    al_map_rgb(255,203,0)

#define SCREEN_W 800
#define SCREEN_H 600


#include "Tribe.h"
#include "Message.h"
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>


class Tribe;

class Game {
public:
    static Game& getInstance()
    {
        static Game instance;
        return instance;
    }
    
    void Setup();
    void Cleanup();
    void Draw();
    void Update();
    void ProcessInput(int unichar);
    void ProcessCommand();
    void GhostsMoan();
    void CycleOldMessages(int dir);
    
    ALLEGRO_BITMAP* LoadBitmap(const char *filename);
    
    int strength;
    ALLEGRO_FONT *font, *fontSmall, *fontTitle;
    ALLEGRO_USTR *input;
    ALLEGRO_DISPLAY *display;
    bool gameover, quit, gameWon;
    
    void AddMessage(MSGSPEAKER speaker,  const char *message);
    
private:
    Game() {};
    Tribe *arrTribes;
    Tribe *currentTribe;
    Message **arrMsgs;
    ParticleEngine *particleEngine;
    
    int cycleMessages;
    
    
    /// Audio
    ALLEGRO_SAMPLE *backgroundMusic;
    ALLEGRO_SAMPLE *ghostsSound;
    bool loadedBackgroundMusic, loadedGhostsSound;
    
    Game(Game const&)=delete;
    void operator=(Game const&)=delete;
    
    
    
    int GetNumber(const char *str, int length);
    void BeginTurn();
    void EndTurn();


};

#endif /* defined(__eldersofethos__game__) */
