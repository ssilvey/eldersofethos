//
//  particle.h
//  eldersofethos
//
//  Created by Steven Silvey on 6/27/15.
//
//

#ifndef __eldersofethos__particle__
#define __eldersofethos__particle__

#include <stdio.h>
#include <allegro5/allegro.h>

typedef enum particleSpirit {
    PARTICLESPIRIT_NONE = 0,
    PARTICLESPIRIT_NORMAL = 1,
    PARTICLESPIRIT_GHOST = 2,
    PARTICLESPIRIT_ANGEL = 3
}PARTICLESPIRIT;

class Particle {
public:
    float x,y;
    float size;
    float dx, dy;
    bool alive;
    
    bool isGhost;
    int ghostWait;
    int ghostShine;
    
    float current_curve, curve_factor;
    ALLEGRO_COLOR col;
    
    Particle(PARTICLESPIRIT spiritType);
    void Draw();
    void Update();
    
    
private:
    
    
};

#endif /* defined(__eldersofethos__particle__) */
