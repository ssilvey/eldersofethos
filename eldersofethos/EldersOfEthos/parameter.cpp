//
//  parameter.cpp
//  eldersofethos
//
//  Created by Steven Silvey on 6/27/15.
//
//

#include <allegro5/allegro_primitives.h>
#include "parameter.h"
#include "game.h"

Parameter::Parameter(const char *iconFileName) {
    this->stock = 0;
    this->spirits = 0;
    this->progress = 0.0f;
    
    Game *game;
    this->icon = game->getInstance().LoadBitmap(iconFileName);
    
}


void Parameter::Draw(const char *caption, int x, int y) {
    Game *game;
    al_draw_text(game->getInstance().fontSmall, FONT_BASIC_COLOR, x+18, y+37,ALLEGRO_ALIGN_CENTER, caption);
    ALLEGRO_USTR *strEnergy = al_ustr_newf("%d", this->spirits);
    al_draw_text(game->getInstance().fontSmall, FONT_BASIC_COLOR, x+18, y+48,ALLEGRO_ALIGN_CENTER, al_cstr(strEnergy));
    al_ustr_free(strEnergy);
    
    float val = this->progress;
    if(this->progress > 1.0f) {
        val = 1.0f;
    }
    else if(this->progress <= 0.0f) {
        val = 0.1f;
    }
    
    al_draw_filled_rectangle(x+35, y+35, x+40.0f, y+35-(35.0f * val), al_map_rgb(255, 203*val, 0));
    al_draw_bitmap(this->icon,x,y,0);

}