//
//  main.cpp
//  eldersofethos
//
//  Created by Steven Silvey on 6/26/15.
//
//

#include <stdio.h>
#include <allegro5/allegro.h>
#include "allegro5/allegro_image.h"
#include "allegro5/allegro_font.h"
#include "allegro5/allegro_ttf.h"
#include "allegro5/allegro_native_dialog.h"
#include "game.h"

const float FPS = 60;
const int BOUNCER_SIZE = 32;

int main(int argc, char **argv){
    ALLEGRO_DISPLAY *display = NULL;
    ALLEGRO_EVENT_QUEUE *event_queue = NULL;
    ALLEGRO_TIMER *timer = NULL;
    ALLEGRO_BITMAP *bouncer = NULL;
    float bouncer_x = SCREEN_W / 2.0 - BOUNCER_SIZE / 2.0;
    float bouncer_y = SCREEN_H / 2.0 - BOUNCER_SIZE / 2.0;
    float bouncer_dx = -4.0, bouncer_dy = 4.0;
    bool redraw = true;
    
    
    /// Initialize Code
    if(!al_init()) {
        fprintf(stderr, "failed to initialize allegro!\n");
        return -1;
    }
    
    if(!al_init_image_addon()) {
        al_show_native_message_box(display, "Error", "Error", "Failed to initialize al_init_image_addon!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        return 0;
    }
    
    if(!al_init_font_addon()) {
        al_show_native_message_box(display, "Error", "Error", "Failed to initialize al_init_font_addon!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        return 0;
    }
    
    if(!al_init_ttf_addon()) {
        al_show_native_message_box(display, "Error", "Error", "Failed to initialize al_init_ttf_addon!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        return 0;
    }
    
    if(!al_install_keyboard()) {
        al_show_native_message_box(display, "Error", "Error", "Failed to initialize al_install_keyboard!", NULL, ALLEGRO_MESSAGEBOX_ERROR);

        return -1;
    }
    
    
    
    if(!al_install_audio()){
        fprintf(stderr, "failed to initialize audio!\n");
        return -1;
    }
    
    if(!al_init_acodec_addon()){
        fprintf(stderr, "failed to initialize audio codecs!\n");
        return -1;
    }
    
    if (!al_reserve_samples(10)){
        fprintf(stderr, "failed to reserve samples!\n");
        return -1;
    }
    
    
    /// Music from here:  http://indiegamemusic.com/viewtrack.php?id=345
    /// SFX from here:  http://freesound.org/people/Johnc/sounds/20683/
    
    
    
    
    
    
    
    timer = al_create_timer(1.0 / FPS);
    if(!timer) {
        fprintf(stderr, "failed to create timer!\n");
        return -1;
    }
    
    display = al_create_display(SCREEN_W, SCREEN_H);
    if(!display) {
        fprintf(stderr, "failed to create display!\n");
        al_destroy_timer(timer);
        return -1;
    }
    
    bouncer = al_create_bitmap(BOUNCER_SIZE, BOUNCER_SIZE);
    if(!bouncer) {
        fprintf(stderr, "failed to create bouncer bitmap!\n");
        al_destroy_display(display);
        al_destroy_timer(timer);
        return -1;
    }
    
    al_set_target_bitmap(bouncer);
    
    al_clear_to_color(al_map_rgb(255, 0, 255));
    
    al_set_target_bitmap(al_get_backbuffer(display));
    
    event_queue = al_create_event_queue();
    if(!event_queue) {
        fprintf(stderr, "failed to create event_queue!\n");
        al_destroy_bitmap(bouncer);
        al_destroy_display(display);
        al_destroy_timer(timer);
        return -1;
    }
    
    al_register_event_source(event_queue, al_get_display_event_source(display));
    
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    
    al_register_event_source(event_queue, al_get_keyboard_event_source());
    
    al_clear_to_color(al_map_rgb(0,0,0));
    
    al_flip_display();
    
    al_start_timer(timer);
    
    Game *game;
    game->getInstance().Setup();
    
    game->getInstance().display = display;

    
    
    /// Test Grounds...
//    ALLEGRO_PATH *path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
//    al_append_path_component(path, "Resources");
//    al_set_path_filename(path, "test2.png");
//    const char *test = al_path_cstr(path, ALLEGRO_NATIVE_PATH_SEP);
//    const char *test2 = al_path_cstr(path, '/');
//    if(al_filename_exists(test2)) {
//        fprintf(stderr, "hello world\n");
//    }
//    
//    fprintf(stderr, "%s\n", test2);
//    ALLEGRO_BITMAP *image = al_load_bitmap(test2);
//    
//    if(!image) {
//        al_show_native_message_box(display, "Error", "Error", "Failed to load image!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
//        al_destroy_display(display);
//        return 0;
//    }
//    
//    al_draw_bitmap(image,200,200,0);
//    al_flip_display();
    
    /// FONTS
    game->getInstance().font = al_load_ttf_font("chintzy.ttf",16,0 );
    
    if (!game->getInstance().font){
        fprintf(stderr, "Could not load 'chintzy.ttf'.\n");
        return -1;
    }
    
    game->getInstance().fontSmall = al_load_ttf_font("chintzy.ttf",14,0 );
    
    if (!game->getInstance().fontSmall){
        fprintf(stderr, "Could not load 'chintzy.ttf'.\n");
        return -1;
    }
    
    game->getInstance().fontTitle = al_load_ttf_font("chintzy.ttf",40,0 );
    
    if (!game->getInstance().fontTitle){
        fprintf(stderr, "Could not load 'chintzy.ttf'.\n");
        return -1;
    }
    
    
    
    
    
    bool doexit = false;
    while(!doexit)
    {
        ALLEGRO_EVENT ev;
        al_wait_for_event(event_queue, &ev);
        
        if(ev.type == ALLEGRO_EVENT_TIMER) {
            game->getInstance().Update();
            doexit = game->getInstance().quit;
//            if(bouncer_x < 0 || bouncer_x > SCREEN_W - BOUNCER_SIZE) {
//                bouncer_dx = -bouncer_dx;
//            }
//            
//            if(bouncer_y < 0 || bouncer_y > SCREEN_H - BOUNCER_SIZE) {
//                bouncer_dy = -bouncer_dy;
//            }
//            
//            bouncer_x += bouncer_dx;
//            bouncer_y += bouncer_dy;
            
            redraw = true;
        }
        else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
            break;
        }
        else if(ev.type == ALLEGRO_EVENT_KEY_UP) {
            switch(ev.keyboard.keycode) {
                case ALLEGRO_KEY_ESCAPE:
                    doexit = true;
                    break;
                case ALLEGRO_KEY_UP:
                    game->getInstance().CycleOldMessages(1);
                    break;
                case ALLEGRO_KEY_DOWN:
                    game->getInstance().CycleOldMessages(-1);
                    break;
            }
        }
        else if(ev.type == ALLEGRO_EVENT_KEY_CHAR) {
            game->getInstance().ProcessInput(ev.keyboard.unichar);
            fprintf(stderr, "%d", ev.keyboard.unichar);
        }
        
        if(redraw && al_is_event_queue_empty(event_queue)) {
            redraw = false;
            
            al_clear_to_color(al_map_rgb(0,0,0));
            
//            al_draw_bitmap(bouncer, bouncer_x, bouncer_y, 0);
            
            
            game->getInstance().Draw();
            
            al_flip_display();
        }
    }
    
    game->getInstance().Cleanup();
    al_destroy_bitmap(bouncer);
    al_destroy_timer(timer);
    al_destroy_display(display);
    al_destroy_event_queue(event_queue);
    
    return 0;
}