//
//  game.cpp
//  eldersofethos
//
//  Created by Steven Silvey on 6/26/15.
//
//

#include <ctype.h>
#include <tgmath.h>
#include <allegro5/allegro_primitives.h>

#include "allegro5/allegro_native_dialog.h"
#include "game.h"

#define MAX_TRIBES 2
#define MAX_MESSAGES 20
#define MAX_MESSAGES_TO_SHOW 6



void Game::Setup() {
    strength = 0;
    
    arrTribes = new Tribe[MAX_TRIBES];
    for(int ndx = 0; ndx < MAX_TRIBES; ndx++) {
        if(ndx != 0) {
            arrTribes[ndx].isCPU = true;
        }
    }
    currentTribe = &arrTribes[0];
    
    input = al_ustr_new("");
    
    
    this->arrMsgs = new Message*[MAX_MESSAGES];
    for(int ndx = 0; ndx < MAX_MESSAGES; ndx++) {
        arrMsgs[ndx] = NULL;
    }
    this->AddMessage(MSGSPEAKER_NONE, "");
    this->AddMessage(MSGSPEAKER_MASLOW, "Welcome, Elder, to Planes of Ethos, the Spirit World.");
    this->AddMessage(MSGSPEAKER_MASLOW, "Every 1,000 years, ten are allowed into Heaven, but from only one tribe.  You can speak 'HOW' to learn more or 'PROCEED' to end your turn:");
    
    this->particleEngine = new ParticleEngine();
    
    this->quit = false;
    this->gameover = false;
    this->gameWon = false;
    
    
    /// Audio
    const char *filename = "space.wav";
    ALLEGRO_PATH *path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
    al_append_path_component(path, "Resources");
    al_set_path_filename(path, filename);
    const char *musicPath = al_path_cstr(path, '/');
    if(!al_filename_exists(musicPath)) {
        fprintf(stderr, "Could not locate file: %s\n", musicPath);
    }
    
    this->backgroundMusic = al_load_sample(musicPath);
    
    if (!this->backgroundMusic){
        al_show_native_message_box(display, "Error", "Error", "The application could not load the background music and will continue without it.", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        this->loadedBackgroundMusic = false;
    }
    else {
        this->loadedBackgroundMusic = true;
        /* Loop the sample until the display closes. */
        al_play_sample(this->backgroundMusic, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_LOOP,NULL);
    }
    
    const char *filename2 = "moan.wav";
    ALLEGRO_PATH *path2 = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
    al_append_path_component(path2, "Resources");
    al_set_path_filename(path2, filename2);
    const char *moanPath = al_path_cstr(path2, '/');
    if(!al_filename_exists(moanPath)) {
        fprintf(stderr, "Could not locate file: %s\n", moanPath);
    }
    
    this->ghostsSound = al_load_sample(moanPath);
    
    if (!this->ghostsSound){
        al_show_native_message_box(display, "Error", "Error", "The application could not load the ghosts sfx and will continue without it.", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        this->loadedGhostsSound = false;
    }
    else {
        this->loadedGhostsSound = true;
    }
    
    
    
}

void Game::GhostsMoan() {
    if(this->loadedGhostsSound) {
        al_play_sample(this->ghostsSound, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_ONCE,NULL);
    }
    
}

void Game::Cleanup() {
    al_ustr_free(this->input);
    for(int ndx = 0; ndx < MAX_MESSAGES; ndx++) {
        if(this->arrMsgs[ndx] != NULL) {
            this->arrMsgs[ndx]->Cleanup();
        }
    }

}


void Game::Draw() {
    
    /// Background
    for(int ndx = 0; ndx < SCREEN_H; ndx+=5) {
        al_draw_filled_rectangle(0.0f, ndx, SCREEN_W, ndx+5, al_map_rgb(0,26.0f-ndx*(26.0f/SCREEN_H),64.0f-ndx*(64.0f/SCREEN_H)));
    }
    
    this->particleEngine->Draw();
    
    al_draw_text(this->fontTitle, FONT_TITLE_COLOR, SCREEN_W/2, 50,ALLEGRO_ALIGN_CENTER, "Elders of Ethos");
    al_draw_text(this->fontSmall, FONT_MASLOW_SPEAKING, SCREEN_W/2, 90,ALLEGRO_ALIGN_CENTER, "Version 1.1");
    
    
    
    this->currentTribe->Draw();
    
    
    for(int ndx = 0; ndx < MAX_MESSAGES_TO_SHOW; ndx++) {
        if(this->arrMsgs[ndx] != NULL) {
            if(ndx+1 < MAX_MESSAGES && this->arrMsgs[ndx+1] != NULL && this->arrMsgs[ndx]->speaker != this->arrMsgs[ndx+1]->speaker) {
                al_draw_text(this->font, this->arrMsgs[ndx]->colName,20, 555-(ndx*20),ALLEGRO_ALIGN_LEFT, this->arrMsgs[ndx]->getSpeakerName());
            }
            
            al_draw_text(this->font, this->arrMsgs[ndx]->col,90, 555-(ndx*20),ALLEGRO_ALIGN_LEFT, al_cstr(this->arrMsgs[ndx]->text));
        }
        
    }
    
    al_draw_text(this->font, FONT_BASIC_COLOR,20, 575,ALLEGRO_ALIGN_LEFT, al_cstr(this->input));
}

void Game::Update() {
    
    this->particleEngine->Update(this->arrTribes[0].SpiritCount()*40.0f, this->arrTribes[0].death->spirits*40.0f, this->arrTribes[0].angel->spirits);
//    this->particleEngine->Update(1000);
}

void Game::ProcessInput(int unichar) {
    if(unichar >= 32 && unichar < 127) {
        al_ustr_append_chr(input, tolower(unichar));
    }
    else if(unichar == 127) {
        al_ustr_remove_chr(input, (int)al_ustr_length(input)-1);
    }
    else if(unichar == 13) {
        this->cycleMessages = 0;
        this->AddMessage(MSGSPEAKER_ELDER, al_cstr(this->input));
        this->ProcessCommand();
        al_ustr_remove_range(input, 0, (int)al_ustr_length(input));
    }

}

void Game::CycleOldMessages(int dir) {
    if(this->arrMsgs[this->cycleMessages] != NULL) {
        if(this->arrMsgs[this->cycleMessages]->speaker == MSGSPEAKER_MASLOW) {
            this->cycleMessages += dir;
            this->CycleOldMessages(dir);
        }
        else {
            al_ustr_remove_range(this->input, 0, (int)al_ustr_length(this->input));
            al_ustr_append(this->input, this->arrMsgs[this->cycleMessages]->text);
        }
        
    }
    
    this->cycleMessages += dir;
    if(this->cycleMessages >= MAX_MESSAGES) {
        this->cycleMessages = MAX_MESSAGES-1;
    }
    if(this->arrMsgs[this->cycleMessages] == NULL) {
        this->cycleMessages -= dir;
    }
    if(this->cycleMessages < 0) {
        this->cycleMessages = 0;
    }
    
    
    
}

void Game::ProcessCommand() {
    
    if(al_ustr_find_cstr(this->input, 0, "quit") == 0 || al_ustr_find_cstr(this->input, 0, "end") == 0) {
        this->quit = true;
    }
    if(this->gameover) {
        this->AddMessage(MSGSPEAKER_MASLOW, "Sorry, Elder, the gates are closed and none shall go to Heaven for quite   some time.  Please say 'quit' or 'end' and try again in 1000 years.");
        return;
    }
    
    if(this->gameWon) {
        this->AddMessage(MSGSPEAKER_MASLOW, "Elder, our spirits are well.  You can continue to manage them for a while (challenge factor bumped up), but none will become ANGELS.  Please say 'quit' or 'end' when you are done.");

    }
    
    
    if(al_ustr_find_cstr(this->input, 0, "free") == 0) {
        TRIBEPARAMETER from = this->currentTribe->FindParameterInUstrInRange(this->input, 0, (int)al_ustr_length(this->input));
        int units = this->GetNumber(al_cstr(this->input), (int)al_ustr_length(this->input));
        
        if(units > 0 && from != TRIBE_PARAMETER_NONE) {
            this->currentTribe->AssignSpirits(units, from, TRIBE_PARAMETER_AVAILABLE);
            this->currentTribe->energy->stock--;
            this->AddMessage(MSGSPEAKER_MASLOW, "By your command, these spirits are now available for repurposing.");
        }
        else {
            this->AddMessage(MSGSPEAKER_MASLOW, "To use the 'free' command, say something like 'FREE 3 from ENERGY'.");
        }
    }
    else if(al_ustr_find_cstr(this->input, 0, "promote") == 0) {
        /// getting hacky...move arrPlanes to public to get er done
        TRIBEPARAMETER from = this->currentTribe->FindParameterInUstrInRange(this->input, 0, (int)al_ustr_length(this->input));
        int units = this->GetNumber(al_cstr(this->input), (int)al_ustr_length(this->input));
        
        if(units > 0) {
            if(units > this->currentTribe->arrPlanes[PLANE_PHYSIOLOGICAL].ready) {
                units =this->currentTribe->arrPlanes[PLANE_PHYSIOLOGICAL].ready;
            }
            if(units > this->currentTribe->arrPlanes[PLANE_PHYSIOLOGICAL].available - this->currentTribe->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling) {
                units =this->currentTribe->arrPlanes[PLANE_PHYSIOLOGICAL].available - this->currentTribe->arrPlanes[PLANE_PHYSIOLOGICAL].unwilling;
            }
            
            if(units > 0) {
                this->currentTribe->arrPlanes[PLANE_SELFACTUAL].available += units;
                this->currentTribe->arrPlanes[PLANE_PHYSIOLOGICAL].available -= units;
                this->currentTribe->arrPlanes[PLANE_PHYSIOLOGICAL].ready -= units;
                this->AddMessage(MSGSPEAKER_MASLOW, "By your command, spirits are now on the SELF-ACTUALIZATION plane.  Assign them to PURITY to increase our chances of spirits ascending to HEAVEN!");
            }
            else {
                this->AddMessage(MSGSPEAKER_MASLOW, "Elder, you do not have any WILLING available spirits that are READY to ascend to SELF-ACTUALIZATION.");
            }
            
        }
        else {
            this->AddMessage(MSGSPEAKER_MASLOW, "To use the 'promote' command, say something like 'PROMOTE 3'.  They must be available and WILLING.");
        }
    }
    else if(al_ustr_find_cstr(this->input, 0, "hello") == 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "Hello, Elder.  You could say 'How do I win', for me to provide you assistance.");
    }
    else if(al_ustr_find_cstr(this->input, 0, "what is energy") == 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "ENERGY is needed to keep a spirit intact.  This ENERGY exists in the natural elements of the cosmos.  Careful, a lacking of ENERGY could cause spirits to disintegrate into PURGATORY.");
    }
    else if(al_ustr_find_cstr(this->input, 0, "what is meditation") == 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "MEDITATION refreshes the spirit, without it, spirits will become UNWILLING to contribute.");
    }
    else if(al_ustr_find_cstr(this->input, 0, "what is guidance") == 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "GUIDANCE encourages the dying souls of the human-world to seek our tribe,  adding new members.");
    }
    else if(al_ustr_find_cstr(this->input, 0, "decay") > 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "FYI, opposing tribes will contribute to DECAY, which increases the factor at which our disciplines deplete.");
    }
    else if(al_ustr_find_cstr(this->input, 0, "evil") > 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "Opposing tribes will contribute to EVIL.  However, it doesn't seem to do anything (this was not coded).");
    }
    else if(al_ustr_find_cstr(this->input, 0, "what is ghosts") == 0 || al_ustr_find_cstr(this->input, 0, "what are ghosts") == 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "GHOSTS are the dead-spirits of PURGATORY.  It requires MIRACLES to bring them out of this pit, which is not currently present in this version of the game.  More ghosts increases the chances of randomly losing the game!");
    }
    else if(al_ustr_find_cstr(this->input, 0, "what is angels") == 0 || al_ustr_find_cstr(this->input, 0, "what are angels") == 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "ANGELS are the goal, Elder.  Recieve 10 of them and our tribe will celebrate your wisdom that led to our tribes' ascendence into HEAVEN.  You will win the game at that point...");
    }
    else if(al_ustr_find_cstr(this->input, 0, "what are the rules") == 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "Seedlings:  fulfilled by caretaking the tribe.");
        this->AddMessage(MSGSPEAKER_MASLOW, "Text Interactivity:  fulfilled by all input via text commands.");
        this->AddMessage(MSGSPEAKER_MASLOW, "Particle Madness:  fulfilled by the spirits in the background.");
        this->AddMessage(MSGSPEAKER_MASLOW, "Sound Annoyance:  if you have GHOSTS, they will make moaning sounds.");
        this->AddMessage(MSGSPEAKER_MASLOW, "Physical Criminality:  this is a game about the spirit-world, in which no physics apply.  Furthermore, one plane is called Physiological.  Criminal!");
    }

    else if(al_ustr_find_cstr(this->input, 0, "what") == 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "I am not sure I understand.  You can say 'what is GUIDANCE' or 'what is ENERGY' or something similar to get more information.  You could also say...'what are the RULES?'");
    }
    else if(al_ustr_find_cstr(this->input, 0, "how do i win") == 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "The goal is to get 10 of our tribe members to become angels.");
    }
    else if(al_ustr_find_cstr(this->input, 0, "who are you") == 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "I am the spirit of Abraham Maslow, your guide.  In my human-life, I created a hierarchy for innate human needs based on priority, culminating in self-actualization.  It turns out the path to the Heavens requires a similiar set of spirit-needs as you see before you.");
    }
    else if(al_ustr_find_cstr(this->input, 0, "who") == 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "I am not sure I understand.  You could say, 'Who are you?'");
    }
    else if(al_ustr_find_cstr(this->input, 0, "how") == 0) {
        this->AddMessage(MSGSPEAKER_MASLOW, "You must balance our focus.  If you have available spirits on a plane, you can say '5 to ENERGY' to reassign them.  To move spirits between dicsiplines, say '8 from GUIDANCE to ENERGY'.  You must say by itself, 'PROCEED', in order for time to progress.");
        this->AddMessage(MSGSPEAKER_MASLOW, "I will do my best to inform you of any important updates within the tribe.");
        
    }
    else if(al_ustr_find_cstr(this->input, 0, "proceed") == 0) {
        this->EndTurn();
    }

    else {
        int posOfTo = al_ustr_find_cstr(this->input, 0, " to ");
        if(posOfTo > 0) {
            int posOfTo2 =al_ustr_find_cstr(this->input, posOfTo+1, " to ");
            if(posOfTo2 > posOfTo) {
                this->AddMessage(MSGSPEAKER_MASLOW, "We are confused by your command, Elder.  Try something like '4 from GUIDAN-CE to ENERGY'.");
                return;
            }
            
            int posOfFrom2 =al_ustr_find_cstr(this->input, posOfTo+1, " from ");
            if(posOfFrom2 > posOfTo) {
                this->AddMessage(MSGSPEAKER_MASLOW, "We are confused by your command, Elder.  Try something like '4 from GUIDAN-CE to ENERGY'.");
                return;
            }

            int units = this->GetNumber(al_cstr(this->input), posOfTo);
            
            if(units > 0) {
                TRIBEPARAMETER from = TRIBE_PARAMETER_AVAILABLE;
                TRIBEPARAMETER to = TRIBE_PARAMETER_NONE;
                
                to = this->currentTribe->FindParameterInUstrInRange(this->input, posOfTo, (int)al_ustr_length(this->input));
                
                int posOfFrom =al_ustr_find_cstr(this->input, 0, " from ");
                if(posOfFrom > 0) {
                    from = this->currentTribe->FindParameterInUstrInRange(this->input, posOfFrom, posOfTo);
                }
                
                int assigned = this->currentTribe->AssignSpirits(units, from, to);
                
                
                if(assigned > 0) {
                    this->currentTribe->energy->stock -= 2;
                    this->AddMessage(MSGSPEAKER_MASLOW, "By your command, WILLING spirits have been reassigned.");
                }
                else {
                    this->AddMessage(MSGSPEAKER_MASLOW, "There are no spirits focused on that discipline to reassign.");
                }
            }
            else {
                this->AddMessage(MSGSPEAKER_MASLOW, "I do not understand your command, Elder.  If you'd like instruction on how to give commands, please say 'How do I play?' Or say 'PROCEED' to have the tribe get to work.");
            }
            
        }
        else {
            this->AddMessage(MSGSPEAKER_MASLOW, "I do not understand your command, Elder.  If you'd like instruction on how to give commands, please say 'How do I play?' Or say 'PROCEED' to have the tribe get to work.");
        }
        
    }
    

    
}

void Game::AddMessage(MSGSPEAKER speaker,  const char *message) {
//    ALLEGRO_USTR *newMessage = al_ustr_new(message);
    Message *newMessage = new Message(al_ustr_new(message), speaker);
//    Message *test = new Message(al_ustr_new(message), al_map_rgb(255,255,255));
    
    /// Find the first occurrence of NULL
    int firstNdxOfNull = MAX_MESSAGES-1;
    for(int ndx = 0; ndx < MAX_MESSAGES; ndx++) {
        if(this->arrMsgs[ndx] == NULL) {
            firstNdxOfNull = ndx;
            break;
        }
    }
    
    if (firstNdxOfNull == MAX_MESSAGES-1) {
        this->arrMsgs[MAX_MESSAGES-1]->Cleanup();
    }
    
    /// Move the messages back in the queue
    for(int ndx = firstNdxOfNull; ndx > 0; ndx--) {
        this->arrMsgs[ndx] = this->arrMsgs[ndx-1];
    }
    
    
    if(al_ustr_length(newMessage->text) > 75) {
        Message *part1 = new Message(al_ustr_dup_substr(newMessage->text, 0, 75), speaker);
//        Message *part2 = new Message(al_ustr_dup_substr(newMessage->text, 31, (int)al_ustr_length(newMessage->text)), speaker);
        arrMsgs[0] = part1;
        this->AddMessage(speaker, al_cstr(al_ustr_dup_substr(newMessage->text, 75, (int)al_ustr_length(newMessage->text))));
        
    }
    else {
        arrMsgs[0] = newMessage;
    }
    
    
}

int Game::GetNumber(const char *str, int length) {
    ALLEGRO_USTR *number = al_ustr_new("");
    int value = 0;
    for(int ndx = 0; ndx < length; ndx++ ) {
        if(str[ndx] >= 48 && str[ndx] < 58) {
            al_ustr_append_chr(number, str[ndx]);
        }
        else {
            if(al_ustr_length(number) > 0) {
                break;
            }
            else {
                al_ustr_remove_range(number, 0, (int)al_ustr_length(number));
            }
        }
    }
    
    /// This string should only have numerical characters now
    if(al_ustr_length(number) > 0) {
        int multiplier = (int)al_ustr_length(number)-1;
        const char *charNumber = al_cstr(number);
        for(int ndx = 0; charNumber[ndx] != '\0'; ndx++) {
            value += pow(10, multiplier--) * (charNumber[ndx]-48);
        }
    }
    return value;
}

void Game::BeginTurn() {
#warning "Currently skipping AI turns"
    if (this->currentTribe->isCPU) {
        fprintf(stderr, "Current tribe is CPU");
        this->EndTurn();
        return;
    }
    
    
    this->currentTribe->UpdateParameters();
    const char *evtMessage = this->currentTribe->DetermineEvent();
    if(evtMessage) {
        this->AddMessage(MSGSPEAKER_MASLOW, evtMessage);
    }
    
}

void Game::EndTurn() {
    
    /// Update the Current Tribe
    int ndx = 0;
    while(&this->arrTribes[ndx] != this->currentTribe) {
        ndx++;
    }
    ndx++;
    if(ndx == MAX_TRIBES) {
        ndx = 0;
    }
    this->currentTribe = &this->arrTribes[ndx];
    
    this->BeginTurn();
}

ALLEGRO_BITMAP* Game::LoadBitmap(const char *filename) {
    ALLEGRO_PATH *path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
    al_append_path_component(path, "Resources");
    al_set_path_filename(path, filename);
    const char *energyIconPath = al_path_cstr(path, '/');
    if(!al_filename_exists(energyIconPath)) {
        fprintf(stderr, "Could not locate file: %s\n", energyIconPath);
    }
    ALLEGRO_BITMAP *image = al_load_bitmap(energyIconPath);
    
    if(!image) {
        al_show_native_message_box(this->display, "Error", "Error", "Failed to load image!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        al_destroy_display(this->display);
    }
    
    return image;
}
