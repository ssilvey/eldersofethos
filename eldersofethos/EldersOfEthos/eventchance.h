//
//  eventchance.h
//  eldersofethos
//
//  Created by Steven Silvey on 6/27/15.
//
//

#ifndef __eldersofethos__eventchance__
#define __eldersofethos__eventchance__

#include <stdio.h>

typedef enum eventCode {
    EVENTCODE_NONE = 0,
    EVENTCODE_DEATH = 1,
    EVENTCODE_GUIDANCE_BOOST = 2,
    EVENTCODE_NEW_UNWILLINGNESS = 3,
    EVENTCODE_NOTHING = 4,
    EVENTCODE_NEW_WILLINGNESS =5,
    EVENTCODE_SPIRITS_MIGRATING = 6,
    EVENTCODE_NEW_SPIRITS = 7,
    EVENTCODE_MEDITATION_BOOST = 8,
    EVENTCODE_LOSE_GAME = 9,
    EVENTCODE_SPIRITS_READY = 10,
    EVENTCODE_ASCEND = 11,
    EVENTCODE_WIN_GAME = 12,
    EVENTCODE_NEW_GHOST = 13,
    EVENTCODE_COSMIC_RIFT = 14,
    EVENTCODE_DECAY_TRIBE = 15,
    EVENTCODE_EVIL_TRIBE = 16

} EVENTCODE;

class EventChance {
public:
    EVENTCODE code;
    int start;
    int end;
    
    EventChance(EVENTCODE code, int start, int end);
    
private:
};

#endif /* defined(__eldersofethos__eventchance__) */
