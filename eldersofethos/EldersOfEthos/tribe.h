//
//  tribe.h
//  eldersofethos
//
//  Created by Steven Silvey on 6/26/15.
//
//

#ifndef __eldersofethos__tribe__
#define __eldersofethos__tribe__

#include <stdio.h>
#include "game.h"
#include "plane.h"
#include "parameter.h"
#include "eventchance.h"


#define PLANE_HEAVEN 3
#define PLANE_SELFACTUAL 2
#define PLANE_PHYSIOLOGICAL 1
#define PLANE_PURGATORY 0


typedef enum tribeParameter {
    TRIBE_PARAMETER_NONE = 0,
    TRIBE_PARAMETER_AVAILABLE = 1,
    TRIBE_PARAMETER_ENERGY = 2,
    TRIBE_PARAMETER_MEDITATION = 3,
    TRIBE_PARAMETER_GUIDANCE = 4,
    TRIBE_PARAMETER_ANY = 5,
    TRIBE_PARAMETER_DEATH = 6,
    TRIBE_PARAMETER_EARTH = 7,
    TRIBE_PARAMETER_OTHER = 8,
    TRIBE_PARAMETER_PURITY = 9,
    TRIBE_PARAMETER_HEAVEN = 10
} TRIBEPARAMETER;



class Tribe
{
public:
    
    /// Heaven
    Parameter *angel;
    
    /// Self-Actualization
    Parameter *purity;
    Parameter *evil;
    
    /// Physiological
    Parameter *decay;
    Parameter *guidance;
    Parameter *energy;
    Parameter *meditation;
    
    
    /// Purgatory
    Parameter *death;
    
    bool isCPU;
    
    Tribe();
    void Draw();
    void UpdateParameters();
    const char* DetermineEvent();
    int AssignSpirits(int count, TRIBEPARAMETER from, TRIBEPARAMETER to);
    TRIBEPARAMETER FindParameterInUstrInRange(ALLEGRO_USTR *ustr, int start, int end);
    int SpiritCount();

    Plane *arrPlanes;
private:
    
    const char *name;
    
    
    
    int DeterminePlane(TRIBEPARAMETER parameter);
    void UpdateProgress();
    void UpdateEnergy();
    void UpdateMeditation();
    void UpdateGuidance();
    void UpdatePurity();
    const char* ProcessEvent(EventChance *event);
    int AvailableSpiritCount();
    void KillRandomSpirit();
    bool RandomlyDetermineUnwillingSpirit();
    bool RandomlyDetermineWillingSpirit();
    
};

#endif /* defined(__eldersofethos__tribe__) */
