//
//  message.cpp
//  eldersofethos
//
//  Created by Steven Silvey on 6/27/15.
//
//

#include "message.h"
#include "game.h"


Message::Message(ALLEGRO_USTR *text, MSGSPEAKER speaker) {
    this->text = al_ustr_dup(text);
    this->speaker = speaker;
    
    switch (speaker) {
        case MSGSPEAKER_MASLOW:
            this->col = FONT_MASLOW_SPEAKING;
            this->colName = FONT_MASLOWNAME_SPEAKING;
            break;
        case MSGSPEAKER_ELDER:
            this->col = FONT_ELDER_SPEAKING;
            this->colName = FONT_ELDERNAME_SPEAKING;
            break;
            
        default:
            break;
    }
}

void Message::Cleanup() {
//    al_ustr_free(this->text);
}

const char* Message::getSpeakerName() {
    switch (this->speaker) {
        case MSGSPEAKER_ELDER:
            return "Elder:";
            break;
        case MSGSPEAKER_MASLOW:
            return "Maslow:";
            break;
            
        default:
            break;
    }
    
    return "";
}
