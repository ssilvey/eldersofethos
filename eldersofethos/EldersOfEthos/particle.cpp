//
//  particle.cpp
//  eldersofethos
//
//  Created by Steven Silvey on 6/27/15.
//
//

#include <allegro5/allegro_primitives.h>
#include <math.h>
#include "particle.h"
#include "game.h"

Particle::Particle(PARTICLESPIRIT spiritType) {
    this->x = arc4random() % SCREEN_W;
    this->y = SCREEN_H;
    this->dy = (float)(arc4random() % 50) / -25.0f - 1;
    this->dx = arc4random() % 3 - arc4random() % 3;
    this->size = arc4random() % 3 + 1;
    
    this->isGhost = false;
    switch (spiritType) {
        case PARTICLESPIRIT_NORMAL:
            this->col = al_map_rgb(0, 29, arc4random() % 105 + 30);
            break;
        case PARTICLESPIRIT_GHOST:
            this->col = al_map_rgb(arc4random() % 50 + 75, 0, arc4random() % 50 + 75);
            this->size += 1;
            this->ghostWait = arc4random() % 100 + 20;
            this->isGhost = true;
            this->ghostShine = 0;
            break;
        case PARTICLESPIRIT_ANGEL:
            this->col = al_map_rgb(arc4random() % 55 + 200, arc4random() % 55 + 200, arc4random() % 55 + 200);
            this->size += 4;
            break;
            
        default:
            break;
    }
    
    this->alive = true;
    
    
    this->current_curve = 0.0f;
    this->curve_factor = arc4random() % 5 / 100.0f;
    
    /// Anomaly
    if(arc4random() % 100 > 98) {
        this->col = al_map_rgb(65, 90, 0);
        this->size = 3;
        this->dx /= 2.0f;
        this->curve_factor /= 2.0f;
    }

}

void Particle::Draw() {
    
    if(this->ghostShine <= 3 || this->ghostShine >= 180) {
        al_draw_filled_circle(this->x-this->dx*4, this->y-this->dy*4, this->size, this->col);
        al_draw_filled_circle(this->x, this->y, this->size+1, al_map_rgb(0, 20, 150));
        al_draw_filled_circle(this->x, this->y, this->size, this->col);
        
    }
    else {
        al_draw_filled_circle(this->x-this->dx*4, this->y-this->dy*4, this->size, al_map_rgb(arc4random() % 20 + 235, arc4random() % 20 + 235, arc4random() % 20 + 235));
        al_draw_filled_circle(this->x, this->y, arc4random() % 4 + this->size, al_map_rgb(arc4random() % 20 + 235, arc4random() % 20 + 235, arc4random() % 20 + 235));
        
    }
}

void Particle::Update() {
    this->x += this->dx;
    this->y += this->dy;
    
    this->dx = sin(this->current_curve);
    
    this->current_curve += this->curve_factor;
    
    if(this->y < 0) {
        this->alive = false;
    }
    
    if(this->isGhost) {
        this->ghostWait--;
        if(this->ghostShine <= 0 && this->ghostWait <= 0) {
            this->ghostShine = 1;
            
            Game *game;
            game->getInstance().GhostsMoan();
        }
        else {
            this->ghostShine++;
            if(this->ghostShine > 300) {
                this->ghostShine = 0;
            }
        }
        
    }
    
}
