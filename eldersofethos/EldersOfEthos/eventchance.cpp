//
//  eventchance.cpp
//  eldersofethos
//
//  Created by Steven Silvey on 6/27/15.
//
//

#include "eventchance.h"

EventChance::EventChance(EVENTCODE code, int start, int end) {
    this->code = code;
    this->start = start;
    this->end = end;
}
