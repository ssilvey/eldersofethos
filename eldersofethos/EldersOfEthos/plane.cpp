//
//  plane.cpp
//  eldersofethos
//
//  Created by Steven Silvey on 6/26/15.
//
//

#include "plane.h"
#include "game.h"


Plane::Plane() {
    available = 0;
    unwilling = 0;
    ready = 0;
    assigned = 0;
}


void Plane::DrawSpiritsInfoAt(int ypos) {
    Game *game;
    
    al_draw_text(game->getInstance().font, FONT_BASIC_COLOR, 475, ypos+10,ALLEGRO_ALIGN_LEFT, "SPIRITS:");
    DrawParameter(game->getInstance().fontSmall, 565, ypos+5, "Available:", this->available);
    DrawParameter(game->getInstance().fontSmall, 565, ypos+20, "Unwilling:", this->unwilling);
    DrawParameter(game->getInstance().fontSmall, 565, ypos+35, "Ready:", this->ready);
    
}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Plane::DrawParameter(ALLEGRO_FONT *font, int xpos, int ypos, const char *caption, int value) {
    al_draw_text(font, FONT_BASIC_COLOR, xpos, ypos,ALLEGRO_ALIGN_LEFT, caption);
    ALLEGRO_USTR *strValue = al_ustr_newf("%d", value);
    al_draw_text(font, FONT_BASIC_COLOR, xpos+90, ypos,ALLEGRO_ALIGN_LEFT, al_cstr(strValue));
    al_ustr_free(strValue);
}