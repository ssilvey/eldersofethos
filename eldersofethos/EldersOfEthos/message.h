//
//  message.h
//  eldersofethos
//
//  Created by Steven Silvey on 6/27/15.
//
//
// test

#ifndef __eldersofethos__message__
#define __eldersofethos__message__

#include <stdio.h>
#include <allegro5/allegro.h>

typedef enum msgSpeaker {
    MSGSPEAKER_NONE = 0,
    MSGSPEAKER_MASLOW = 1,
    MSGSPEAKER_ELDER = 2
    
} MSGSPEAKER;

class Message {
    
public:
    ALLEGRO_USTR *text;
    ALLEGRO_COLOR col, colName;
    MSGSPEAKER speaker;

    Message(ALLEGRO_USTR *text, MSGSPEAKER speaker);
    void Cleanup();
    const char* getSpeakerName();
    
private:
    
};

#endif /* defined(__eldersofethos__message__) */
