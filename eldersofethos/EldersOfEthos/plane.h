//
//  plane.h
//  eldersofethos
//
//  Created by Steven Silvey on 6/26/15.
//
//

#ifndef __eldersofethos__plane__
#define __eldersofethos__plane__

#include <stdio.h>
struct ALLEGRO_FONT;

class Plane
{
public:
    int available;
    int unwilling;
    int ready;
    int assigned;
    
    Plane();
    void DrawSpiritsInfoAt(int ypos);
    
private:
    void DrawParameter(ALLEGRO_FONT *font, int xpos, int ypos, const char *caption, int value);
    
};

#endif /* defined(__eldersofethos__plane__) */
